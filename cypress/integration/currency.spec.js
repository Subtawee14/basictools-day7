/// < reference type = "Cypress" />
describe('Fill the form Test', () => {

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })


    it('visit CAMT web site', function () {
        cy.visit('https://currency-exchange-969bb.web.app/currency')

        let currency = ['USD', 'THB', 'EUR'];

        // for (var i = 0; i < currency.length; i++) {
           
        //     cy.get('#selectSource').click()
        //     let source = '#source' + currency[i]
        //     cy.get(source).click()

        //     for (var j = 0; j < currency.length; j++) {
        //         cy.get('#sourceAmount').clear()
        //         let money = Math.floor(Math.random() * 100);
        //         cy.get('#sourceAmount').type(money)
        //         console.log(currency[i] + " to " + currency[j])
        //         let target = '#target' + currency[j]
        //         cy.get('#selectTarget').click()
        //         cy.get(target).click()
        //         cy.get('#calBtn').click()
        //     }
        // }



        //Input 0
        cy.get('#sourceAmount').type(0)
        cy.get('#selectSource').click()
        cy.get("#sourceUSD").click()
        cy.get('#selectSource').click()
        cy.get('#selectTarget').click()
        cy.get("#targetTHB").click()
        cy.get('#calBtn').click()
        cy.get('#result').should('contain', '0.00 THB')
        

        cy.get('#sourceAmount').clear()

        //input-1
        // cy.get('#sourceAmount').type(-1)
        // cy.get('#selectSource').click()
        // cy.get("#sourceUSD").click()
        // cy.get('#selectSource').click()
        // cy.get('#selectTarget').click()
        // cy.get("#targetTHB").click()
        // cy.get('#calBtn').click()
        // cy.get('#result').should('contain', 'Error')


        // cy.get('#sourceAmount').clear()

        // //input a
        // cy.get('#sourceAmount').type('a')
        // cy.get('#selectSource').click()
        // cy.get("#sourceUSD").click()
        // cy.get('#selectSource').click()
        // cy.get('#selectTarget').click()
        // cy.get("#targetTHB").click()
        // cy.get('#calBtn').click()
        // cy.get('#result').should('contain', 'Error')


        cy.get('#sourceAmount').clear()

        //input 2.05
        cy.get('#sourceAmount').type(2.05)
        cy.get('#selectSource').click()
        cy.get("#sourceUSD").click()
        cy.get('#selectSource').click()
        cy.get('#selectTarget').click()
        cy.get("#targetTHB").click()
        cy.get('#calBtn').click()
        cy.get('#result').should('contain', '61.50 THB')


    })

})