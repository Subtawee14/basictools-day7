/// < reference type = "Cypress" />
describe('My First Test', function () {
    it('visit CAMT web site', function () {
        cy.visit('https://camt.cmu.ac.th/th')
        cy.contains('ทุนการศึกษา').click()
    })
    it('เปิดหน้าโครงสร้างวิทยาลัย', function () {
        cy.visit('https://camt.cmu.ac.th/th')
        cy.contains('เกี่ยวกับเรา').click()
        cy.contains('โครงสร้างวิทยาลัยฯ').click()
        cy.get('#Image130').click()
    })

    it('เปิดหน้าอาจารย์โต', function () {
        cy.visit('https://camt.cmu.ac.th/th')
        cy.contains('เกี่ยวกับเรา').click()
        cy.contains('โครงสร้างวิทยาลัยฯ').click()
        cy.get('#Image135').click()
        cy.contains('ดร.ชาติชาย ดวงสอาด').click()
        
    })

    it('Open my CV page', function () {
        cy.visit('https://dv-student-profile.web.app/')
        cy.get('body > div > div.table-responsive > table > tbody > tr:nth-child(10) > td:nth-child(5) > a').should('have.attr', 'href', 'firstCV/Subtawee_5009/cv.html')
        cy.get('body > div > div.table-responsive > table > tbody > tr:nth-child(10) > td:nth-child(5) > a').click()
        
    })
})